# -*- coding: utf-8 -*-

import logging
import os
import re

from gi.repository import GObject, Gtk

from .config import Config

APP_NAME = 'WikiOutline'
FILE_DIR = os.path.dirname(__file__)

logger = logging.getLogger(APP_NAME)
logger.setLevel(logging.DEBUG)

wikiOutlineConfig = Config({
    'schema_name': 'org.gnome.gedit.plugins.wikioutline',
    'schema_dir': os.path.join(FILE_DIR, 'data', 'schema'),
    'ui_path': os.path.join(FILE_DIR, 'data', 'ui', 'config.glade'),
    'domain': 'wikioutline'
})


class WikiOutlineManager(object):

    def __init__(self):
        self._model = Gtk.TreeStore(str, int)
        self._expanded_rows = []
        self._selected_refs = []

    def get_model(self):
        '''Returns TreeStore'''
        return self._model

    def store_treeview_state(self, treeview):
        # store expanded rows
        self._expanded_rows = []
        treeview.map_expanded_rows(self._expanded_rows_callback, None)
        # store selection
#        model, paths = treeview.get_selection().get_selected_rows()
#        self._selected_refs = []
#        if paths is not None:
#            for path in paths:
#                self._selected_refs.append(Gtk.TreeRowReference.new(self._model, path))

    def restore_treeview_state(self, treeview):
        # restore expanded rows
        for strpath in self._expanded_rows:
            path = Gtk.TreePath.new_from_string(strpath)
            if path:
                treeview.expand_row(path, False)
        # restore selection
#        for ref in self._selected_refs:
#            path = ref.get_path()
#            if path is not None:
#                treeiter = self._model.get_iter(path)
#                if treeiter is not None:
#                    treeview.get_selection().select_iter(treeiter)

    def _expanded_rows_callback(self, treeview, path, data):
        self._expanded_rows.append(str(path))


class WikiOutline(GObject.GObject):
    '''Outline ui/model'''

    __gsignals__ = {
        'header-selected': (GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE, (GObject.TYPE_INT,)),
    }

    re_atx_header  = re.compile('^(?P<level>#{1,6})\s*(?P<title>.+)(\s+#{1,})?$')

    def __init__(self):
        GObject.GObject.__init__(self)
        self._file_extensions = []
        self._path = None
        self._cache = {None: WikiOutlineManager()}
        self._ignore = []
        self.atx_mode = False

        builder = Gtk.Builder()
        builder.add_objects_from_file(
            os.path.join(FILE_DIR, 'data', 'ui', 'pane.glade'),
            ['outline_pane']
        )
        self._widget = builder.get_object('outline_pane')
        self._list = builder.get_object('outline_tree')
        builder.connect_signals(self)

        self._update_settings()
        wikiOutlineConfig.settings.connect('changed', self.on_settings_changed)

    def get_widget(self):
        '''returns outline widget'''
        return self._widget

    def set_file_extensions(self, extensions: str):
        '''define file extensions (comma separated)'''
        self._file_extensions = extensions

    def is_valid_extension(self, extension: str) -> bool:
        '''Is the given extension valid ?'''
        return extension in self._file_extensions

    def is_valid_path(self, path: str) -> bool:
        '''is path valid (path of a file with valid extension) ?'''
        root, ext = os.path.splitext(path)
        if ext != '':
            return self.is_valid_extension(ext[1:])
        return False

    def is_active(self) -> bool:
        '''Is widget active ?'''
        return self._widget.get_sensitive()

    def update(self, path, text=None):
        '''Update outline for the given path.'''
        keypath = None if path in self._ignore else path
        if keypath != self._path:
            if keypath is not None and keypath not in self._cache:
                if not self.is_valid_path(keypath):
                    self._ignore.append(path)
                    keypath = None
                else:
                    self._cache[keypath] = WikiOutlineManager()
        self._update_state(keypath)

        if self.is_active() and text is not None:
            self._parse(text)

    def _update_state(self, path):
        '''Update state, storing current treeview state before updating it to a
        new state.'''
        if self._path is not None:
            manager = self._cache[self._path]
            manager.store_treeview_state(self._list)
        if self._path != path:
            self._path = path
            manager = self._cache[self._path]
            self._list.set_model(manager.get_model())
            manager.restore_treeview_state(self._list)
            self._widget.set_sensitive(path is not None)

    def _parse(self, text):
        manager = self._cache[self._path]
        manager.store_treeview_state(self._list)
        model = manager.get_model()
        model.clear()
        parents = []
        levels = []
        i = 0
        title = None
        start_level = -1

        for j, line in enumerate(text.splitlines()):
            i = -1
            if line.startswith('#'):
                m = self.re_atx_header.match(line.strip())
                if m:
                    i = len(m.group('level')) - 1
                    title = replace_wiki_markup(m.group('title').strip())

            if i > -1:
                if start_level == -1:
                    start_level = i
                i = i - start_level
                while i > len(parents):
                    parents.append(model.append(parents[-1] if parents else None,
                                    ('empty node', j-1)))
                if i < len(parents):
                    parents = parents[:i]

                parents.append(model.append(parents[-1] if parents else None,
                                (title, j-1)))
            title = line

        manager.restore_treeview_state(self._list)

    def _update_settings(self):
        settings = wikiOutlineConfig.settings
        ext = settings.get_string('extensions')
        if ext != '':
            ext = [x.strip() for x in ext.split(',')]
        else:
            ext = []
        self.set_file_extensions(ext)

    def on_tree_row_activated(self, treeview, path, column):
        '''row activation listener'''
        rowiter = treeview.get_model().get_iter(path)
        line = treeview.get_model().get_value(rowiter, 1)
        self.emit('header-selected', line)

    def on_settings_changed(self, settings, key, data=None):
        '''Settings listener'''
        self._update_settings()


WIKI_MARKUP = ['{{', '}}', 'øø', '**', '__', '``', '??', '--']

def replace_wiki_markup(s : str):
    '''Replace wiki markup'''
    for markup in WIKI_MARKUP:
        s = s.replace(markup, '')
    return s
