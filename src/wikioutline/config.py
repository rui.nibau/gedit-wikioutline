# -*- coding: utf-8 -*-

import os

from gi.repository import Gio, Gtk
from gi.repository.Gdk import RGBA
from gi.repository.GLib import VariantClass

def get_settings(schema_name, schema_dir=None):
    '''
    Charger un objet de configuration Gio.Settings.

    La méthode cherche d'abord si la configuration demandée est chargé globalement.
    Si ce n'est pas le cas, elle tentera de charger un schéma 'local', présent
    dans le répertoire 'schema_dir'.

    @param str schema_name: Nom du schéma.
    @param str schema_dir: répertoire du schéma (None par défaut).
    @return Gio.Settings | None
    '''
    schemas = Gio.Settings.list_schemas()
    if schema_name in schemas:
        return Gio.Settings.new(schema_name)
    if schema_dir is not None and os.path.isdir(schema_dir):
        schema_source = Gio.SettingsSchemaSource.new_from_directory(
            schema_dir, 
            Gio.SettingsSchemaSource.get_default(), 
            False
        )
        schema = schema_source.lookup(schema_name, False)
        if schema is not None:
            return Gio.Settings.new_full(schema, None, None)
    return None


def get_details(schema_path, schema_name, translation=False):
    '''
    @param  str     schema_path Chemin du fichier schema
    @param  str     schema_name Nom du schéma
    @return dict    Dictionnaire des clés du schéma avec leur résumé et leur
                    descriptions. Le dictionnaire peut être vide s'il y a
                    eut un problème de lecture du fichier xml ou si le
                    schéma n'existe pas.
    '''
    from xml.dom import minidom

    res = {}
    try:
        xml = minidom.parse(schema_path)
        schemas = xml.getElementsByTagName('schema')
        schema = None

        for node in schemas:
            if node.getAttribute('id') == schema_name:
                schema = node
                break

        if schema is None:
            raise Exception('No schema named ' + schema_name)

        keys = schema.getElementsByTagName('key')
        for key in keys:
            details = {}
            # get summary and description
            try:
                details['summary'] = key.getElementsByTagName('summary')[0].childNodes[0].data
            except:
                details['summary'] = ''
            try:
                details['description'] = key.getElementsByTagName('description')[0].childNodes[0].data
            except:
                details['description'] = ''

            # get type
            details['type'] = get_type(key.getAttribute('type'))
            
            if details['type'] == 'string':
                # try to find choices
                choices = key.getElementsByTagName('choice')
                if choices.length > 0:
                    for choice in choices:
                        choice.getAttribute('value')
            elif details['type'] == 'numeric':
                # try to find range
                try:
                    range_element = key.getElementsByTagName('range')[0]
                    # TODO To numeric value ?
                    details['range'] = {
                        'min': range_element.getAttribute('min'),
                        'max': range_element.getAttribute('max')
                    }
                except:
                    pass

            res[key.getAttribute('name')] = details
    
    except:
        print('ERROR')
        #logging.error('Error while parsing GSettings schema <%s>', schema_path)

    return res


def get_type(vtype):
    if vtype == 's':
        return 'string'
    if vtype == 'i':
        return 'numeric'
    if vtype == 'b':
        return 'boolean'
    return 'string'


class Config(object):
    '''
    Objet permettant de gérer une fenètre de configuration à partir d'un
    objet de préférences gsettings.
    '''

    '''
    Liste de dictionnaire liant un type de préférences GSettings, un type de 
    widget GTK et une propriété de ce widget.

    [[http://blog.kosmokaryote.org/2013/01/writing-rhythmbox-plugin-rhythmbox-dj.html]]
    pour le mapping choice.
    '''
    mapper = [
        {'type': 'boolean' , 'widget': 'Switch'        , 'prop': 'active'},
        {'type': 'boolean' , 'widget': 'CheckButton'   , 'prop': 'active'},
        {'type': 'choice'  , 'widget': 'ComboBoxText'  , 'prop': 'active-id'},
        {'type': 'numeric' , 'widget': 'SpinButton'    , 'prop': 'value'},
        {'type': 'string'  , 'widget': 'Entry'         , 'prop': 'text'},
        {'type': 'string'  , 'widget': 'FontButton'    , 'prop': 'font-name'}
    ]

    # Cache des mapping par widgets
    widgets_mapper = {}
    
    # Cache des mapping par type
    types_mapper = {}

    def __init__(self, data=None):
        '''Création d'un objet de configuration
        @param dict [data = None] Liste des données:
            * str  schema_name Nom du chemin gsettings
            * str  [schema_dir = None] Chemin du fichier de compilation GSettings
            * str  [domain = None] Domaine pour la localisation
            * str  [widget_id = 'config_widget']
            * str  [dialog_id = 'config_dialog']
            * list [ids]
            * bool [options = False]
        '''
        self.data = data if data is not None else {}
        
        if not 'schema_name' in self.data:
            raise ValueError('A Gsettings schema name must be provided')
        if not 'schema_dir' in self.data:
            self.data['schema_dir'] = None
        if not 'domain' in self.data:
            self.data['domain'] = None
        if not 'widget_id' in self.data:
            self.data['widget_id'] = 'config_widget'
        if not 'dialog_id' in self.data:
            self.data['dialog_id'] = 'config_dialog'
        if not 'options' in self.data:
            self.data['options'] = False

        self.settings = get_settings(self.data['schema_name'], self.data['schema_dir'])
        self.options = {}
        
        if self.data['options'] is True:
            keys = self.settings.list_keys()
            if len(keys) > 0:
                for key in keys:
                    self._update_option(key)
            keys = None
            self.settings.connect('changed', self.on_settings_changed)

    def on_settings_changed(self, settings, key, data=None):
        '''Ecoute des changements de préférences'''
        self._update_option(key)
    
    def on_clear_event(self, widget):
        '''Listening to 'clear events', events to clear settings. user data
        MUST BE the widget that is mapped to the key that must be cleared.'''
        if widget:
            key = widget.get_name()
            if key:
                self.settings.reset(key)
                wtype = type(widget).__name__
                if wtype == 'ColorButton':
                    self.on_color_update(widget, key)

    def on_color_set(self, widget, data=None):
        '''Ecoute de la définition d'une couleur sur un composant ColorButton'''
        color = widget.get_rgba()
        key = widget.get_name()
        self.settings.set_string(key, color.to_string())
        if self.data['options'] is True:
            self._update_option(key, color)
    
    def on_color_update(self, widget, key):
        '''Update color widget has it can't be bound to a settings key'''
        value = self.settings.get_string(key)
        rgba = RGBA()
        if value:
            rgba.parse(value)
        widget.set_rgba(rgba)


    def get_options(self):
        return self.options

    def get_mapping_from_type(self, pref_type):
        if pref_type not in self.types_mapper:
            self.types_mapper[pref_type] = None
            for mapping in self.mapper:
                if mapping['type'] == pref_type:
                    self.types_mapper[pref_type] = mapping
                    break
        return self.types_mapper[pref_type]

    def get_mapping_from_widget(self, widget_name):
        if widget_name not in self.widgets_mapper:
            self.widgets_mapper[widget_name] = None
            for mapping in self.mapper:
                if mapping['widget'] == widget_name:
                    self.widgets_mapper[widget_name] = mapping
                    break
        return self.widgets_mapper[widget_name]

    def create_widget(self):
        '''Récupérer le contenu du panneau de configuration.
        return: Gtk.Box
        '''
        if 'ui_path' in self.data:
            return self._create_from_builder(self.data['widget_id'])
        return self._create_from_details(self.data['widget_id'])

    def create_dialog(self):
        '''Récupérer une fenêtre de dialogue
        return: Gtk.Dialog
        '''
        if 'ui_path' in self.data:
            return self._create_from_builder(self.data['dialog_id'])
        return self._create_from_details(self.data['dialog_id'])

    def _create_from_builder(self, object_id):
        ''''''
        ids = []
        if 'ids' in self.data:
            ids = self.data['ids']
        ids.append(object_id)
        builder = Gtk.Builder()
        builder.add_objects_from_file(self.data['ui_path'], ids)
        self._bind_builder(builder)
        self._connect_builder(builder)
        return builder.get_object(object_id)

    def _create_from_details(self, object_id):
        #self._details = get_details(self._cfg['schemapath'], self.settings['schema-id'])
        raise NotImplementedError('Not ready yet')

    def _connect_builder(self, builder):
        '''Connection du builder
        @param Gtk.Builder builder
        '''
        if 'connect' in self.data:
            if self.data['connect'] is True:
                builder.connect_signals(self)
            else:
                builder.connect_signals(self.data['connect'])

    def _bind_builder(self, builder):
        '''Bind des widgets du panneaux de configuration
        @param Gtk.Builder builder
        '''
        keys = self.settings.list_keys()
        if len(keys) < 1:
            return
        for key in keys:
            widget = builder.get_object(key)
            if widget is None:
                print('No widget named {0}'.format(key))
                continue
            wtype = type(widget).__name__
            mapping = self.get_mapping_from_widget(wtype)
            prop = None
            if mapping:
                # Automatic binding
                prop = mapping['prop']
                self.settings.bind(key, widget, prop, Gio.SettingsBindFlags.DEFAULT)
            elif wtype == 'ColorButton':
                # Manual binding: color
                # Widget MUST HAVE 'on_color_set' listening 'color-set' event
                self.on_color_update(widget, key)
            else:
                print('No mapping for a {0} widget'.format(wtype))
        keys = None
    

                    
    def _update_option(self, key, value=None):
        '''Mise à jour des options.
        @param String key: Nom de la clé
        '''
        if value is None:
            value = self.settings.get_value(key)
            classy = value.classify()
            if classy == VariantClass.BOOLEAN:
                value = value.get_boolean()
                if 'bool_option' in self.data:
                    value = self.data['bool_option'](key, value)
            elif classy == VariantClass.INT32:
                value = value.get_int32()
                if 'int_option' in self.data:
                    value = self.data['int_option'](key, value)
            elif classy == VariantClass.STRING:
                value = value.get_string()
                if 'str_option' in self.data:
                    value = self.data['str_option'](key, value)

        if value is not None:
            self.options[key] = value
        elif key in self.options:
            del self.options[key]

