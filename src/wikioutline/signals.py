# -*- coding: utf-8 -*-
#
#   signals.py
#
#   Inspired by bzrpanel plugin and multiedit plugin
#

class Signals():

    def __init__(self):
        self._signals = {}
        self._signals_blocked = {}

        self._handlers = {}
        self._htogglers = {}

    def __do_connect(self, connector, obj, name, handler, *args):
        ret = self._signals.setdefault(obj, {})
        hid = connector(name, handler, *args)
        ret.setdefault(name, []).append(hid)
        return hid

    def _connect(self, obj, name, handler, *args):
        return self.__do_connect(obj.connect, obj, name, handler, *args)

    def _connect_after(self, obj, signal, handler, *args):
        return self.__do_connect(obj.connect_after, obj, name, handler, *args)

    def _disconnect(self, obj, name=None):
        if obj not in self._signals:
            return False

        if name is None:
            for name in self._signals[obj]:
                for hid in self._signals[obj][name]:
                    obj.disconnect(hid)
            del self._signals[obj]
            return True

        if name not in self._signals[obj]:
            return False

        for hid in self._signals[obj][name]:
            obj.disconnect(hid)
        del self._signals[obj][name]

        if len(self._signals[obj]) == 0:
            del self._signals[obj]

        return True

    def _disconnect_all(self):
        if len(self._signals) == 0:
            return False

        for obj in self._signals:
            for name in self._signals[obj]:
                for hid in self._signals[obj][name]:
                    obj.disconnect(hid)

        self._signals.clear()
        return True
