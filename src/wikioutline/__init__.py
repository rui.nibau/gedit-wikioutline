# -*- coding: utf-8 -*-

from gi.repository import GObject, Gtk, Gedit, PeasGtk, Tepl

from .signals import Signals
from .wikioutline import WikiOutline, wikiOutlineConfig

NAME = 'WikiOutline'

class WikiOutlineWindowActivatable(GObject.Object, 
                                    Gedit.WindowActivatable, 
                                    PeasGtk.Configurable,
                                    Signals):
    '''
    Plugin permettant d'afficher les titres wiki du document courant
    sous forme d'outline dans un panneau latéral.
    '''
    __gtype_name__ = 'WikiOutlineWindowActivatable'

    window = GObject.property(type=Gedit.Window)

    def __init__(self):
        GObject.Object.__init__(self)
        Signals.__init__(self)
        self._outline = None
        self._tab_changed = False

    def do_create_configure_widget(self):
        '''ui Configuration'''
        return wikiOutlineConfig.create_widget()

    def do_activate(self):
        '''Plugin activation'''
        self._outline = WikiOutline()
        self._connect(self._outline, 'header-selected', self.on_header_selected)
        self._tab_changed = False
        self._add_pane()

    def do_deactivate(self):
        '''Plugin deactivation'''
        self._disconnect_all()
        self._remove_pane()
        self._outline = None
        self.window = None

    def _add_pane(self):
        side_panel = self.window.get_side_panel()
        pane = self._outline.get_widget()
        if isinstance(side_panel, Tepl.Panel):
            Tepl.Panel.add(side_panel, pane, NAME, 'Outline', None)
        else:
            side_panel.add_titled(pane, NAME, 'Outline')
        self._connect(side_panel, 'hide', self.on_hide_side_panel)
        self._connect(side_panel, 'show', self.on_show_side_panel)
        self._connect(side_panel, 'changed', self.on_side_panel_changed)

    def _remove_pane(self):
        side_panel = self.window.get_side_panel()
        side_panel.remove(self._outline.get_widget())

    def _update(self):
        doc = self.window.get_active_document()
        path = None
        text = None
        if doc is not None and doc.get_file().get_location() is not None:
            path = doc.get_file().get_location().get_path()
        if self._tab_changed and path is not None and self._outline.is_valid_path(path):
            bounds = doc.get_bounds()
            text = doc.get_text(bounds[0], bounds[1], True).replace('\r', '\n') + '\n'
        self._outline.update(path, text)

    def on_header_selected(self, outline, line):
        '''Selection listener'''
        view = self.window.get_active_view()
        doc = self.window.get_active_document()
        iter = doc.get_iter_at_line(line)
        view.scroll_to_iter(iter, 0.0, True, 0.0, 0.0)

    def on_side_panel_changed(self, container, data=None):
        '''Side panel changed listener'''
        self.on_hide_side_panel(None)
        if container.get_active_item_name() == NAME:
            self.on_show_side_panel(None)

    def on_hide_side_panel(self, panel):
        '''Side panel closed: disconnect listeners'''
        self._disconnect(self.window)

    def on_show_side_panel(self, panel):
        '''Side panel opened: attach listeners'''
        self._connect(self.window, 'active-tab-state-changed', self.on_active_tab_state_changed)
        self._connect(self.window, 'active-tab-changed', self.on_active_tab_changed)
        self._tab_changed = True
        self._update()

    def on_active_tab_changed(self, window, data=None):
        '''Active tab changed: update outline'''
        state = window.get_active_tab().get_state()
        if state == Gedit.TabState.STATE_NORMAL:
            self._tab_changed = False
            self._update()
        else:
            self._tab_changed = True

    def on_active_tab_state_changed(self, window, data=None):
        '''tab state changed: update outline'''
        state = window.get_active_tab().get_state()
        if state == Gedit.TabState.STATE_LOADING:
            self._tab_changed = True
        elif self._outline.is_active() and state == Gedit.TabState.STATE_SAVING:
            self._tab_changed = True
        elif state == Gedit.TabState.STATE_NORMAL:
            if self._tab_changed:
                self._update()
                self._tab_changed = False
