---
title:      Historique
date:       2012-05-20
updated:    2024-05-01
---

°°changelog°°
2024-05-01:
    • fix: [tepl 6 compatibility](https://gedit-technology.github.io/developer-docs/libgedit-tepl-6/api-breaks-during-tepl-6.html)
2024-02-24:
    • fix: remove wiki markup on title
    • fix: [#1](https://framagit.org/rui.nibau/gedit-wikioutline/-/issues/1) Outline not displayed when left panel is closed at start
    • del: remove obsolete configurations (setext titles)
    • upd: clean code (pylint)
2023-12-03:
    • fix: Gedit 46 compatibility.
2021-04-08 (40):
    • fix: Disparition de l'API ``Gedit.Document.goto_line()``.
2020-05-01:
    • fix: Disparition de l'API ``Gedit.Document.get_location()``, remplacée par ``Gedit.Document.get_file().get_location()``.
2017-04-01:
    • fix: Sauvegarde des états plié/déplié des noeuds de l'arbre.
    • add: Prendre comme niveau supérieur de titre le premier niveau rencontré dans le texte. Cela évite d'avoir un noeud fictif racine.
2017-01-28:
    • add: Gestion de la [Syntaxe atx](http://www.aaronsw.com/2002/atx/intro) sur 6 niveaux avec un préfixe construit par une série de croisillons (« # »).
2014-04-27 (3.12):
    • upd: Adaptation à Gedit 3.12.
2014-04-05 (3.9):
    • upd: Meilleur organisation du code.
    • upd: Dépendances par fichier et non plus par module 'common'.
2013-07-06 (3.8):
    • upd: Adaptation à gedit 3.8 (python 3).
2013-05-25 (3.6):
    • upd: Adoption d'une numérotation principale identique à Gedit.
    • upd: Restructuration du plugin.
    • upd: Utilisation du module GSettings.
    • fix: Corrections dans la gestion des extensions.
2013-04-01 (0.4):
    • upd: Première version diffusée.
2012-05-20 (0.3):
    • upd: Première version pour Gedit 3. Non diffusé.
2011-08-05 (0.2):
    • fix: Corrections et gestion de formats de fichiers supplémentaires.
2011-08-01 (0.1):
    • add: Première version pour Gedit 2. Non diffusé.

