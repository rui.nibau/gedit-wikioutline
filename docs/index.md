---
title:      Gedit : WikiOutline
banner:     /lab/gedit-wikioutline/pics/banner.png
date:       2012-05-20
updated:    2024-05-01
cats:       [informatique]
tags:       [gedit, plugin, syntaxe-wiki, wikioutline]
techs:      [python, gtk]
source:     https://framagit.org/rui.nibau/gedit-wikioutline
issues:     https://framagit.org/rui.nibau/gedit-wikioutline/-/issues
status:     in-process
version:    2024-05-01
download:   /lab/gedit-wikioutline/build/latest.zip
contact:    true
itemtype:   SoftwareApplication
pagesAsSnippets: true
intro: WikiOutline est un plugin pour Gedit ≥ 40 permettant de visusaliser la hiérarchie d'un document texte dont le contenu est écrit avec une syntaxe wiki type markdown.
---

°°|install°°
## Installation

**WARN : For 3.12 ≤ Gedit < 40, use [gedit-wikioutline-2020-05-01](gedit-wikioutline-2020-05-01.tar.gz)**

..include:: /data/snippets/gedit-plugin-install.md
    plugin-name = wikioutline


°°|usage°°
## Utilisation

°°pic sz-full pos-centre°°
----
![°°splash°°Capture d'écran](/lab/gedit-wikioutline/pics/2024-02-24.png)
----
WikiOultine 2024-02-24 pour Gedit > 40.

Une fois installé, le plugin vérifie si le document courant dans Gedit est d'un format qu'il peut gérer. Si c'est le cas, il essayera de constuire une hiérarchie de titres.

Les titres peuvent suivre la syntaxe setext ou la syntaxe atx.

°°|config°°
## Configuration

°°pic pos-right sz-half°°
----
![Image](/lab/gedit-wikioutline/pics/2024-02-24-config.png)
----
Panneau de configuration de WikiOutline 2024-02-24.

Le panneau de configuration du plugin permet de définir la liste d'extensions de fichiers (séparés par une virgule) devant être traité par WikiOultine sachant que ceux ayant un type MIME ``text/wiki`` sont implicitement analysés.

°°|history°°
## Historique

..include::./changelog.md

°°|licence°°
## Licence

..include::./licence.md

