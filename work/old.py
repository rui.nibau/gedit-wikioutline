#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import gedit
import gconf

import gc
import time

class WikiHeaderParser (object):
    '''
    Updates a treemodel, adding entries for each header section
    '''
    def __init__(self, treemodel):
        self.treemodel = treemodel
        
    def parse(self, text, header_chars):
        last = None
        parents = []
        levels  = []
        for j,line in enumerate(text.splitlines()):
            first = line[0] if line else ''
            line = line.strip()
            if (last and 
                first in header_chars and 
                len(line) > 1 and len(line) > 10 and
                all(c==first for c in line)):
                if first not in levels:
                    levels.append(first)
                i = levels.index(first)                
                while i>len(parents):
                    parents.append(self.treemodel.append(parents[-1] if parents else None, 
                                         ('empty node', str(j-1)) ))
                if i<len(parents):
                    parents = parents[:i]
                parents.append(self.treemodel.append(parents[-1] if parents else None, 
                                  (last, str(j-1)) ))
            last = line


class OutlineBox(gtk.VBox):

    def __init__(self):
        gtk.VBox.__init__(self)

        scrolledwindow = gtk.ScrolledWindow()
        scrolledwindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrolledwindow.show()
        self.pack_start(scrolledwindow, True, True, 0)

        self.treeview = gtk.TreeView()
        self.treeview.set_rules_hint(True)
        self.treeview.set_headers_visible(False)
        self.treeview.set_enable_search(True)
        self.treeview.set_reorderable(False)
        self.treeselection = self.treeview.get_selection()
        self.treeselection.connect('changed', self.on_selection_changed)
        scrolledwindow.add(self.treeview)

        col = gtk.TreeViewColumn()
        col.set_title('name')
        col.set_sizing(gtk.TREE_VIEW_COLUMN_AUTOSIZE)
        col.set_expand(True)
        render_text = gtk.CellRendererText()
#        render_text.set_property('cell-background', '#CCCCCC')
        render_text.set_property('xpad', 5)
        render_text.set_property('xalign', 0)
        render_text.set_property('yalign', 0.5)
        col.pack_start(render_text, expand=False)
        col.add_attribute(render_text, 'text', 0)
        
        render_text = gtk.CellRendererText()
        render_text.set_property('xalign', 0)
        render_text.set_property('yalign', 0.5)
        render_text.set_property('visible', False)
        col.pack_start(render_text, expand=True)
        col.add_attribute(render_text, 'text', 1)
        
        self.treeview.append_column(col)
        self.treeview.set_search_column(0)

        self.label = gtk.Label()
        self.pack_end(self.label, False)

        self.expand_classes = False
        self.expand_functions = False

        self.show_all()

    def on_row_has_child_toggled(self, treemodel, path, iter):
        self.treeview.expand_row(path, False)

    def on_selection_changed(self, selection):
        model, iter = selection.get_selected()
        if iter:
            lineno = model.get_value(iter, 1)
            if not lineno: return
            lineno = int(lineno)
            name = model.get_value(iter, 0)
            linestartiter = self.buffer.get_iter_at_line(lineno)
            lineenditer = self.buffer.get_iter_at_line(lineno)
            lineenditer.forward_line()
            lineenditer.backward_char()
            line = self.buffer.get_text(linestartiter, lineenditer)
            self.buffer.select_range(linestartiter, lineenditer)
            self.view.scroll_to_cursor()

    def create_treemodel(self):
        #Header header, lineno
        treemodel = gtk.TreeStore(str, str)
        handler = treemodel.connect('row-has-child-toggled', self.on_row_has_child_toggled)
        return treemodel, handler

    def parse(self, view, buffer, header_chars):
        self.view = view
        self.buffer = buffer

        startTime = time.time()

        treemodel, handler = self.create_treemodel()
        self.treeview.set_model(treemodel)
        self.treeview.freeze_child_notify()

        bounds = self.buffer.get_bounds()
        text   = self.buffer.get_text(bounds[0], bounds[1]).replace('\r', '\n') + '\n'
        wikiparser = WikiHeaderParser(treemodel)
        wikiparser.parse(text, header_chars)
        gc.collect()

        treemodel.disconnect(handler)
        self.treeview.thaw_child_notify()

        stopTime = time.time()
        self.label.set_text('Outline created in ' + str(float(stopTime - startTime)) + ' s')

        return treemodel



class WikiOutlinePluginInstance(object):

    def __init__(self, plugin, window):
        self._window = window
        self._plugin = plugin

        self._insert_panel()
        self._models = {}

    def deactivate(self):
        self._remove_panel

        self._window = None
        self._plugin = None

    def update_ui(self):
        document = self._window.get_active_document()
        if document:
            extensions = self._plugin.get_extensions()
            uri = str(document.get_uri())
            if document.get_mime_type() == 'text/wiki' or any(uri.endswith('.'+ext) for ext in extensions):
                self.outlinebox.parse(self._window.get_active_view(), document, self._plugin.get_header_chars())
            else:
                treemodel, handler = self.outlinebox.create_treemodel()
                self.outlinebox.treeview.set_model(treemodel)

    def _insert_panel(self):
        self.panel = self._window.get_side_panel()
        self.outlinebox = OutlineBox()
        self.panel.add_item(self.outlinebox, "Wiki Outline", gtk.STOCK_INDEX)

    def _remove_panel(self):
        self.panel.destroy()


class WikiOutlinePlugin(gedit.Plugin):

    def __init__(self):
        gedit.Plugin.__init__(self)
        self._gconfdir = '/apps/gedit-2/plugins/wikioutline'
        self._instances = {}
        headers = gconf.client_get_default().get_string(self._gconfdir+"/headers")
        ext = gconf.client_get_default().get_string(self._gconfdir+"/extensions")
        if headers is None:
            self._header_chars = ['=', '-', '.', "'", '"']
        else:
            self._header_chars = headers.split(' ')
        if ext is None:
            self._extensions = ['md', 'txt', 'wiki', 'css']
        else:
            self._extensions = ext.split(',')

    def activate(self, window):
        self._instances[window] = WikiOutlinePluginInstance(self, window)

    def deactivate(self, window):
        self._instances[window].deactivate()
        gconf.client_get_default().set_string(self._gconfdir+"/headers", " ".join(self._header_chars))
        gconf.client_get_default().set_string(self._gconfdir+"/extensions", ",".join(self._extensions))
        del self._instances[window]

    def update_ui(self, window):
        self._instances[window].update_ui()
    
    def get_header_chars(self):
        return self._header_chars
    
    def get_extensions(self):
        return self._extensions
        
    def create_configure_dialog(self):

        dialog = gtk.Dialog("WOP configuration")
        dialog.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
        dialog.connect("response", self.on_response) 
        dialog.set_resizable(False)
        dialog.vbox.set_spacing(5)

        # headings
        for i, char in enumerate(self._header_chars):
            hbox = gtk.HBox(False, 5)
            label = gtk.Label("Header " + str(i+1))
            hbox.add(label)
            text = gtk.Entry(1)
            text.set_name('h'+str(i+1))
            text.set_width_chars(1)
            text.set_text(char)
            hbox.add(text)
            dialog.vbox.add(hbox)

        # extension
        hbox = gtk.HBox(False, 5)
        label = gtk.Label("Extensions")
        hbox.add(label)
        text = gtk.Entry()
        text.set_name('ext')
        text.set_text(",".join(self._extensions))
        hbox.add(text)
        dialog.vbox.add(hbox)
        
        dialog.vbox.show_all()
        
        return dialog
    
    def on_response(self, w, e):
        self._header_chars = []
        # FIXME Vraiment tout pourri
        for i, hbox in enumerate(w.vbox.get_children()):
            children = hbox.get_children()
            if len(children) == 2:
                print children[1].get_name()
                if children[1].get_name() != "ext":
                    self._header_chars.append(children[1].get_text())
                else:
                    self._extensions = children[1].get_text().split(',')
        w.destroy()

