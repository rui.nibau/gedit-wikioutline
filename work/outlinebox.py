# -*- coding: utf-8 -*-

from gi.repository import GObject
from gi.repository import Gtk

class OutlineBox(Gtk.VBox):

    __gsignals__ = {
        'header-selected': (GObject.SIGNAL_RUN_FIRST, GObject.TYPE_NONE, (GObject.TYPE_INT,)),
    }
    
    def __init__(self):
        Gtk.VBox.__init__(self)
        self._lid = None;
        self.expanded_rows = {}
        self._hchars = []
        self._current_uri = None
        self.create_ui()
        self.show_all()
    
    def create_ui(self):
        """ Création de l'interface graphique  """
        sw = Gtk.ScrolledWindow()
        sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        sw.show()
        
        self.tree = Gtk.TreeView()
        self.tree.set_rules_hint(True)
        self.tree.set_headers_visible(False)
        self.tree.set_enable_search(False)
        self.tree.set_reorderable(False)
        self.tree.freeze_child_notify()
        sw.add(self.tree)
        
        self.model = Gtk.TreeStore(str, str)
        self.tree.set_model(self.model)
        
        # TreeViewColumn
        col = Gtk.TreeViewColumn()
        col.set_title('name')
        col.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
        col.set_expand(True)
        
        text = Gtk.CellRendererText()
        text.set_property('xpad', 5)
        text.set_property('xalign', 0)
        text.set_property('yalign', 0.5)
        col.pack_start(text, expand=False)
        col.add_attribute(text, 'text', 0)
        
        text = Gtk.CellRendererText()
        text.set_property('xalign', 0)
        text.set_property('yalign', 0.5)
        text.set_property('visible', False)
        col.pack_start(text, expand=True)
        col.add_attribute(text, 'text', 1)
        
        self.tree.append_column(col)
        self.tree.set_search_column(0)
        
        self.pack_start(sw, True, True, 0)
    
    def set_headerchars(self, chars):
        self._hchars = chars
    
    def dispose(self):
        """ Destruction de l'objet """
        self._disconnect_selection()
    
    def clear(self):
        """ Nettoyer la TreeView """
        self._save_expanded_rows()
        self.model.clear()
        self._current_uri = None
    
    def update(self, text, uri):
        """
        Mettre à jour le modèle.
        @param {String} text Texte à parser
        @param {String} uri Url du fichier courant
        """
        self._disconnect_selection()
        
        self.clear()
        self._current_uri = uri
        
        # parse
        last = None
        parents = []
        levels  = []
        for j, line in enumerate(text.splitlines()):
            first = line[0] if line else ''
            line = line.strip()
            if (last and first in self._hchars and len(line) > 10 and
                    all(c == first for c in line)):
                if first not in levels:
                    levels.append(first)
                i = levels.index(first)
                while i > len(parents):
                    parents.append(self.model.append(parents[-1] if parents else None, 
                                         ('empty node', str(j-1)) ))
                if i < len(parents):
                    parents = parents[:i]
                
                parents.append(self.model.append(parents[-1] if parents else None, 
                                  (last, str(j-1)) ))
            last = line
        
        # expand
        if uri in self.expanded_rows:
            for strpath in self.expanded_rows[uri]:
                path = Gtk.TreePath.new_from_string(strpath)
                if path:
                    self.tree.expand_row(path, False)
        else:
            self.tree.expand_all()
        
        self._connect_selection()
    
    def _connect_selection(self):
        self._lid = self.tree.get_selection().connect('changed', self.on_selection_changed)
    
    def _disconnect_selection(self):
        if self._lid is not None:
            self.tree.get_selection().disconnect(self._lid)
            self._lid = None
    
    def is_current(self, uri):
        """
            Une uri correspond-t-elle au document déjà parsé ?
            @param {String} uri L'uri à tester
            @return Boolean
        """
        return self._current_uri is not None and self._current_uri == uri
    
    def _save_expanded_rows(self):
        """ Sauvegarder l'état déplié des noeuds """
        if self._current_uri:
            self.expanded_rows[self._current_uri] = []
            self.tree.map_expanded_rows(self._save_expanded_rows_mapping_func, self._current_uri)
    
    def _save_expanded_rows_mapping_func(self, treeview, path, uri):
        """ Méthode de callback de la sauvegarde de l'état des noeuds  """
        self.expanded_rows[uri].append(str(path))
    
    def on_selection_changed(self, selection):
        """ Ecouteur de sélection de ligne dans la TreeView """
        model, treeiter = selection.get_selected()
        if treeiter:
            line = model.get_value(treeiter, 1)
            if not line: 
                return
            self.emit('header-selected', int(line))

