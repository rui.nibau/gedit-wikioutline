#!/bin/bash

APPNAME='wikioutline'
VERSION="2024-05-01"

# Dossiers utiles
CURRENT_DIR="$( cd "$( dirname "$0" )" && pwd )"
BUILD_DIR=$CURRENT_DIR"/build/"
SRC_DIR=$CURRENT_DIR"/src/"
DOC_DIR=$CURRENT_DIR"/docs/"

# Builder

echo -e "Build de "$APPNAME

vname='gedit-'$APPNAME"-"$VERSION
varch=$vname".zip"
vdir=$BUILD_DIR$vname"/"
latest=$BDIR"/latest.zip"

# Création du dossier de build dédié
mkdir $vdir

# delete previous archive
if [[ -f $varch ]]; then
    rm $varch
fi
if [[ -f $latest ]]; then
    rm $latest
fi

# Copie du script d'install
if [[ -f $SRC_DIR"plugin.sh" ]]; then
    cp -p $SRC_DIR"plugin.sh" $vdir"plugin.sh"
fi

# copie du fichier plugin.plugin
cp -p -t $vdir $SRC_DIR$APPNAME".plugin"

# traitement du dossier plugin ou fichier plugin.py
cp -prL -t $vdir $SRC_DIR$APPNAME

# Supprimer les fichier *.pyc, dossier __pycache__
find $vdir -type f -name "*.pyc" -delete
find $vdir$APPNAME -type d -name "__pycache__" -delete

# zip
cd $BUILD_DIR
zip -qr $varch $vname
cp -p $varch "latest.zip"

# Suppression du dossier de build
rm -fr $vdir
