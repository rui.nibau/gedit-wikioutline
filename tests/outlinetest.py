#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
from gi.repository import GLib, Gtk
from outlinebox import OutlineBox


CDIR = os.path.dirname(__file__)
FILE_PATH = os.path.join(CDIR, 'test.txt')


class OutlineBoxTest(object):

    def __init__(self):
        self.box = OutlineBox()
        self.box.connect('header-selected', self.on_header_selected)
        self.update_outline()
        window = Gtk.Window(
            title='OutlineBoxTest', 
            window_position=Gtk.WindowPosition.CENTER,
            width_request=400,
            height_request=300
        )
        window.add(self.box);
        window.connect_after('destroy', self.on_destroy)
        window.show_all()
    
    def update_outline(self):
        self.box.header_chars = ['=', '-', '.']
        f = open(FILE_PATH, 'r')
        text = f.read()
        f.close()
        self.box.update(text, FILE_PATH)
    
    def on_header_selected(self, box, line):
        print 'header selected: ' + str(line)
    
    def on_destroy(self, window):
        print 'OutlineBoxTest destroyed'
        Gtk.main_quit()


# Main function: run application
def main():
    test = OutlineBoxTest()
    Gtk.main()


# run main
if __name__ == "__main__":
    sys.exit(main())
